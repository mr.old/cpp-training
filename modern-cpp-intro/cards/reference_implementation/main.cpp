#include "cards.hpp"

#include <map>
#include <iostream>

int main(int argv, char *argc[]) {
    using cards::Card;
    using cards::Deck;

    std::map<Card::Rank, Deck> decks;

    for (std::size_t i = 0; i < 16; ++i) {
        auto deck = Deck::full();
        deck.shuffle();
        auto&& [iterator, inserted] = decks.insert({deck.top().rank(), std::move(deck)});
        if (inserted) {
            iterator->second.shuffle();
            std::cerr << "Rank of new top card after reshuffle: " << static_cast<int>(iterator->second.take().rank()) << "\n";
        } else {
            std::cerr << "Duplicit top card rank.\n";
        }
    }
}
