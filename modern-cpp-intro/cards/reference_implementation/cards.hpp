#ifndef REF_CARDS_HPP
#define REF_CARDS_HPP

#include <algorithm>
#include <array>
#include <random>
#include <vector>
#include <tuple>

namespace cards {

class Card {
public:
    using id_type = std::size_t;

    enum class Rank {
        ACE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING,
        JOKER,
        BLANK,
    };
    enum class Suit {
        HEART,
        DIAMOND,
        CLUBS,
        SPADES,
        BLANK,
    };
    // forbid copying
    Card(const Card&) = delete;
    // public constructors
    constexpr Card() noexcept = default;
    constexpr Card(Card&& other) noexcept {
        swap(other);
    }
    // named constructors
    constexpr static Card blank() noexcept { return Card(); }
    static Card joker() noexcept { return Card(Rank::JOKER, Suit::BLANK); }
    static Card card(Rank rank, Suit suit) noexcept {
        if (suit == Suit::BLANK || rank == Rank::JOKER || rank == Rank::BLANK) {
            return Card::blank();
        }
        return Card(rank, suit);
    }

    // deleted otherwise
    constexpr Card& operator=(Card&& other) noexcept {
        m_id = other.id();
        m_rank = other.rank();
        m_suit = other.suit();
        other.m_id = BLANK_ID;
        other.m_rank = Rank::BLANK;
        other.m_suit = Suit::BLANK;
        return *this;
    }

    constexpr void swap(Card& other) noexcept {
        auto id = m_id;
        auto rank = m_rank;
        auto suit = m_suit;
        m_id = other.m_id;
        m_rank = other.m_rank;
        m_suit = other.m_suit;
        other.m_id = id;
        other.m_rank = rank;
        other.m_suit = suit;
    }

    constexpr Rank rank() const noexcept { return m_rank; }
    constexpr Suit suit() const noexcept { return m_suit; }
    constexpr id_type id() const noexcept { return m_id; }

    friend bool operator<(const Card& lhs, const Card& rhs) noexcept {
        return std::tie(lhs.m_suit, lhs.m_rank) < std::tie(rhs.m_suit, rhs.m_rank);
    }

    friend bool operator==(const Card& lhs, const Card& rhs) noexcept {
        return std::tie(lhs.m_suit, lhs.m_rank) == std::tie(rhs.m_suit, rhs.m_rank);
    }

protected:
    Card(Rank r, Suit s) noexcept : m_rank(r), m_suit(s), m_id(idCounter++) {}

private:
    Rank m_rank {Rank::BLANK};
    Suit m_suit {Suit::BLANK};
    id_type m_id {BLANK_ID};

    inline static id_type idCounter = 0;
    inline static constexpr id_type BLANK_ID = std::numeric_limits<id_type>::max();
};

class Deck {
public:
    Deck() = default;
    static Deck full() {
        Deck deck;
        deck.m_deck.reserve(ranks.size() * suits.size() + 4);
        for (auto suit : suits) {
            for (auto rank : ranks) {
                deck.put(Card::card(rank, suit));
            }
        }
        for (int i = 0; i < 4; ++i) {
            deck.put(Card::joker());
        }
        return deck;
    }

    std::size_t size() const noexcept { return m_deck.size(); }
    bool empty() const noexcept { return m_deck.empty(); }

    void put(Card c, std::size_t position = 0) {
        position = size() - std::min(position, size());
        m_deck.insert(m_deck.begin() + position, std::move(c));
    }
    void put_bottom(Card c, std::size_t position = 0) {
        position = std::min(position, size());
        m_deck.insert(m_deck.begin() + position, std::move(c));
    }

    Card take(std::size_t position = 0) {
        position = size() - 1 - std::min(position, size() - 1);
        Card c = std::move(m_deck[position]);
        m_deck.erase(m_deck.begin() + position);
        return c;
    }
    Card take_bottom(std::size_t position = 0) {
        position = std::min(position, size() - 1);
        Card c = std::move(m_deck[position]);
        m_deck.erase(m_deck.begin() + position);
        return c;
    }

    const Card& top() const noexcept { return m_deck.back(); }
    const Card& bottom() const noexcept { return m_deck.front(); }

    void cut(std::size_t position) noexcept {
        if (position >= size())
            return;

        std::rotate(m_deck.begin(), m_deck.begin() + position, m_deck.end());
    }

    void shuffle() { shuffle(rng()); }

    template <typename URBG>
    void shuffle(URBG&& s) {
        std::shuffle(m_deck.begin(), m_deck.end(), std::forward<URBG>(s));
    }

    std::size_t find(const Card& card) {
        auto it = std::find(m_deck.begin(), m_deck.end(), card);
        return m_deck.end() - it - 1;
    }

    void sort_by_id() {
        std::sort(m_deck.begin(), m_deck.end(),
            [](const Card& lhs, const Card& rhs) {
                return lhs.id() < rhs.id();
            });
    }

private:
    std::vector<Card> m_deck;

    using Rank = Card::Rank;
    using Suit = Card::Suit;
    inline static const std::array<Card::Rank, 13> ranks = {Rank::ACE,
                                                            Rank::TWO,
                                                            Rank::THREE,
                                                            Rank::FOUR,
                                                            Rank::FIVE,
                                                            Rank::SIX,
                                                            Rank::SEVEN,
                                                            Rank::EIGHT,
                                                            Rank::NINE,
                                                            Rank::TEN,
                                                            Rank::JACK,
                                                            Rank::QUEEN,
                                                            Rank::KING};
    inline static const std::array<Card::Suit, 4> suits = {
        Suit::HEART, Suit::DIAMOND, Suit::CLUBS, Suit::SPADES};

    static std::mt19937& rng() {
        static std::mt19937 g(std::random_device{}());
        return g;
    }
};
}  // namespace cards

#endif
