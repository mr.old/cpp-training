#define CATCH_CONFIG_MAIN
#include "Catch2/single_include/catch2/catch.hpp"

#include "cards.hpp"

using namespace cards;
using Rank = Card::Rank;
using Suit = Card::Suit;

TEST_CASE("Card assignment") {
    Card c1 = Card::card(Rank::QUEEN, Suit::DIAMOND);
    Card c2 {std::move(c1)};

    REQUIRE(c1.id() != c2.id());
    REQUIRE(c1.id() == std::numeric_limits<std::size_t>::max());
    REQUIRE(c2.rank() == Rank::QUEEN);
    REQUIRE(c2.suit() == Suit::DIAMOND);

    c1 = Card::card(Rank::QUEEN, Suit::DIAMOND);
    c2 = std::move(c1);

    REQUIRE(c1.id() != c2.id());
    REQUIRE(c1.id() == std::numeric_limits<std::size_t>::max());
    REQUIRE(c2.rank() == Rank::QUEEN);
    REQUIRE(c2.suit() == Suit::DIAMOND);
}

TEST_CASE("Card comparison") {
    auto c1 = Card::card(Rank::QUEEN, Suit::DIAMOND);
    auto c2 = Card::card(Rank::TWO, Suit::DIAMOND);

    REQUIRE(c2 < c1);

    c2 = Card::card(Rank::QUEEN, Suit::DIAMOND);

    REQUIRE(c1 == c2);
}

TEST_CASE("Empty deck") {
    REQUIRE_NOTHROW(Deck{});
}

TEST_CASE("Full deck") {
    Deck d = Deck::full();

    REQUIRE(d.size() == 56);
    
    std::set<Card> known;
    for (int i = 0; i < 56; ++i) {
        known.insert(d.take());
    }
    REQUIRE(known.size() == 53);
    REQUIRE(known.count(Card::blank()) == 0);
    REQUIRE(known.count(Card::joker()) == 1);
}

TEST_CASE("Shuffle") {
    Deck d = Deck::full();
    Deck d2 = Deck::full();

    d.shuffle();
    REQUIRE(d.size() == d2.size());
}

TEST_CASE("Put and take from deck") {
    Deck deck;
    std::vector<Card> cards;
    cards.emplace_back(Card::joker());
    cards.emplace_back(Card::card(Rank::ACE, Suit::HEART));
    cards.emplace_back(Card::card(Rank::KING, Suit::HEART));
    cards.emplace_back(Card::card(Rank::QUEEN, Suit::HEART));
    cards.emplace_back(Card::card(Rank::JACK, Suit::HEART));
    cards.emplace_back(Card::card(Rank::TEN, Suit::HEART));

    std::vector<Card::id_type> ids;
    std::transform(cards.begin(), cards.end(), std::back_inserter(ids),
                   [](auto& c) { return c.id(); });

    std::stringstream ss;
    REQUIRE(ids.size() == cards.size());
    SECTION("Put to top") {
        for (auto&& c : cards) {
            deck.put(std::move(c));
        }
        // { Joker, Ace, King, Queen, Jack, Ten }

        SECTION("Take from top") {
            for (int i = deck.size()-1; i > 0; --i) {
                Card c = std::move(deck.take());
                CHECK(c.id() == ids[i]);
            }
        }
        SECTION("Take from bottom") {
            for (int i = 0; i < deck.size(); ++i) {
                Card c = std::move(deck.take_bottom());
                CHECK(c.id() == ids[i]);
            }
        }
        SECTION("Take from top with index") {
            Card c = deck.take(3); // { Joker, Ace, Queen, Jack, Ten }
            CHECK(c.id() == ids[2]);
            CHECK(c.rank() == Rank::KING);
            REQUIRE(deck.size() == 5);
            c = deck.take(10); // { Ace, Queen, Jack, Ten }
            CHECK(c.rank() == Rank::JOKER);
            REQUIRE(deck.size() == 4);
            CHECK(c.id() == ids[0]);
            c = deck.take(0); // { Ace, Queen, Jack }
            CHECK(c.rank() == Rank::TEN);
            REQUIRE(deck.size() == 3);
            CHECK(c.id() == ids[5]);
        }

        SECTION("Take from bottom with index") {
            Card c = deck.take_bottom(3); // { Joker, Ace, King, Jack, Ten }
            CHECK(c.id() == ids[3]);
            CHECK(c.rank() == Rank::QUEEN);
            REQUIRE(deck.size() == 5);
            c = deck.take_bottom(10); // { Joker, Ace, King, Jack }
            CHECK(c.rank() == Rank::TEN);
            REQUIRE(deck.size() == 4);
            CHECK(c.id() == ids[5]);
            c = deck.take_bottom(0);   // { Ace, King, Jack }
            CHECK(c.rank() == Rank::JOKER);
            REQUIRE(deck.size() == 3);
            CHECK(c.id() == ids[0]);
        }
    }
}
