# příklad - karty
Toto je poloformální specifikace pro implementaci karet a balíčků karet.
Všechny uvedené třídy by měly být umístěny do `namespace cards`.
Cílem tohoto cvičení je osvojit si některé používané přístupy z moderního C++ jako `enum class`, const correctness, správného použití `noexcept`, typy které nelze kopírovat, správné vnoření typů atd.

## class Card;
Třída reprezentuje kartu. Karty mají barvy a hodnoty odpovídající žolíkovým kartám (4 barvy, 13 hodnot + žolík), reprezentovat lze také prázdnou kartu.
Každá karta má kromě barvy a hodnoty také unikátní **výrobní číslo** (dále jen ID; odpovídající typu `std::size_t`).
Kvůli ID karty **nelze kopírovat**. Prázdné karty nemají vlastní ID, obsahují zástupnou hodnotu odpovídající maximální hodnotě.

Operace:
- pojmenované vytvoření běžné karty, žolíka, prázdné karty (implicitní konstruktor) s kontrolou parametrů
- move konstruktor, move přiřazení (po move se očekává prázdná karta na původním místě)
- swap
- zjištění hodnot barvy, hodnoty, ID
- operátory <, ==
- bonus: vlastní stringový literál ""_card (`namespace cards::literals`)

## class Deck;
Třída reprezentuje balíček karet.

Operace:
- Vytvoření prázdného, plného balíčku karet.
- velikost balíčku
- umístění karty na libovolnou pozici z vrchu a/nebo ze spodu balíčku
- odebrání karty z libovolné pozice od vrchu a/nebo ze spodu balíčku
- náhled na vrchní a spodní kartu v balíčku
- "sejmutí" balíčku: od vybrané pozice se část balíčku přesune ze spodu na vrch
- zamíchání balíčku po dodání zdroje náhodných čísel
- zamíchání balíčku s implicitním zdrojem náhodných čísel pro naši třídu (například `std::mt19937`)
- seřazení balíčku podle výrobního čísla karty (nejvyšší v.č. nahoře)
