# Přehled moderního C++

---
**INFO**

V příkladech používám `struct`, abych si ušetřil explicitní `public`, v produkčním kódu to obvykle
není dobrý nápad.

---

## auto
Placeholder pro typ. Skutečný typ se odvodí z typu výrazu inicializace.

```c++
auto pi = 3.14f;
static_assert(std::is_same<decltype(pi),float>::value); // ok, static_assert viz níže
```

`auto` přináší

-   bezpečnější kód - nutná inicializace, no *narrowing conversions*
-   vyšší výkon - no narrowing conversions
-   obecnější kód
  -   snadná změna použitých typů
  -   programuje se proti rozhraním, ne konkrétním typům
-   a mimochodem trochu ušetří psaní (`unique_ptr<widget> w = make_unique<widget>()` vs `auto w = make_unique<widget>()`)

`auto` může být modifikované `const` a `&`, případně může jít o univerzální referenci `auto&&` (viz dále).

Modifikátor `const` ani `&` se automaticky nepropaguje, je nutné je explicitně uvést.

```c++
void g(const int& v)
{
    auto w = v;                 // w je int (ne const int&)
    const auto x = v;           // x je const int
    auto& y = v;                // y je const int&
    const auto& z = v;          // z je také const int&
}
```

### Reference

-   <https://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/>
-   <https://www.fluentcpp.com/2019/07/12/auto-const-smart-pointer-bad-mix/>

## Type aliasing (`using`)
Lepší typedef. Jednotná syntaxe, jako přiřazení (`using nové_jméno = původní_typ;`).

```c++
using int_vec = std::vector<int>;
```

Oproti `typedef` umožňuje definovat i šablonový alias.

```c++
template<class T>
struct Alloc { };
template<class T>
using Vec = vector<T, Alloc<T>>; // type-id is vector<T, Alloc<T>>
Vec<int> v; // Vec<int> is the same as vector<int, Alloc<int>>
```

## Inicializace `{}`

Existuje mnoho způsobů, jak inicializovat, viz např. *slajdy* 4-6 z [Josuttisovy
přednášky](https://meetingcpp.com/mcpp/slides/2019/KeynoteJosuttisMeetingC++181117.pdf). Omezil bych
se na následující.

```c++
X a3 = v;
X a4(v);
X a2 = {v};
X a1 {v};                           // list initialization, novinka od C++11
```

Poslední příklad je nový od C++11. Protože lze použít v každém kontextu, je podle [Stroustrup]
preferovaný.
Taky předchází *narrowing conversions*.

```c++
int j = 1.5;                    // validní kód, ale ztráta přesnosti
//int k {1.5};                  // error: narrowing conversion of ‘1.5e+0’ from ‘double’ to ‘int’ inside { } [-Wnarrowing]
```

*List initialization* lze použít i pro inicializaci vlastní třídy vytvořením konstruktoru na
[initializer list](https://en.cppreference.com/w/cpp/utility/initializer_list).

```c++
struct kolekce {
    kolekce(std::initializer_list<int> l) : v(l) {}
    std::vector<int> v;
};

kolekce k {1, 2, 3, 4, 5};
```

Pro struktury a cokoliv s *initializer list* konstruktorem

```c++
std::vector<std::string> notes {"ugly", "lazy"}; // ["ugly", "lazy"]
auto ints1 = std::vector<int>{3, 2}; // [3, 2]
auto ints2 = std::vector<int>(3, 2); // [2, 2, 2] - použije se `fill-constructor'
```

## Inicializace v `if` a `switch`
Od C++17 je možné definovat a inicializovat proměnné jen pro tělo podmínky/větvení. Proměnné
potřebné jen pro `if`/`switch` nezanáší obalovací *scope*.

```c++
if (auto it = m.find(10); it != m.end()) {
    return it->second.size();
}

switch (Device dev = get_device(); dev.state())
{
    case SLEEP: /*...*/ break;
    case READY: /*...*/ break;
    case BAD: /*...*/ break;
}
```

## noexcept
Klíčové slovo, kterým programátor slíbí, že volání funkce nemůže vyhodit výjimku. Pokud vyýjimu
přesto vyhodí, dojde k `std::terminate`.

## Správa zdrojů
Zdroje

-   paměť,
-   socket
-   mutex
-   DB connection,
-   cokoliv, na co je na konci nutné volat `delete`, `close` atp.

"Zapomeňte" na manuální `new/delete`.

### RAII

-   [RAII](https://en.wikipedia.org/wiki/Resource_acquisition_is_initialization) - resource acquisition is initialisation
-   život alokovaných zdrojů je svázaný se životem vlastníka (objekt, kontejner, ...)
-   vlastník alokuje zdroj při konstrukci a uvolní při destrukci
-   i při výjimce dojde ke správnému uvolnění, nejsou potřebné konstrukce à la `try / catch / finally`

### *Leak free* kód

![Leak freedom](./leakfree.png)

Co lze, to vytvářet na stacku. Při opuštění *scope* se vše uvolní.

```c++
int main() {
    const std::string filename{"abc.txt"};
    std::ifstream istrm(filename, std::ios::binary);
    // práce se souborem ...
} // destruktor ifstream zavře soubor, není nutný explicitní close()
```

Když něco nelze vytvořit na stacku, pak pro bezpečnou (leak free) práci s pamětí použít smart
pointers. Volbou typu smart pointeru určujeme vlastnictví (ownership).

#### Unique pointer
[`unique_ptr`](http://en.cppreference.com/w/cpp/memory/unique_ptr) vlastní odkazovaný objekt. Při
svém zániku smaže i odkazovaný objekt.

```c++
std::unique_ptr<X> make_X()
{
    return std::make_unique<X>();
}

int main(int argc, char* argv[])
{
    auto x = make_X();
}
```

V kódu není memory leak, pole je uvolněné na konci *scope* v `main()`.

`std::make_unique<T>` existuje od C++14. Od C++11 a před C++14 se používalo `std::unique_ptr<T>(new
T())`. Tam ale mohl nastat leak při výjimkách, viz. např. [Exception safety and
make_unique](https://stackoverflow.com/a/19472607) (mělo by být opravené od C++17).

#### Shared pointer
[`shared_ptr`](http://en.cppreference.com/w/cpp/memory/shared_ptr) sdílí vlastnictví. Interně počítá
počet instancí odkazujících na sdílený objekt a při poklesu na 0 odkazovaný objekt smaže.

#### Weak pointer
[`weak_ptr`](http://en.cppreference.com/w/cpp/memory/weak_ptr) lze použít na zjištění, jestli objekt
vlastněný nějakým `shared_ptr` ještě existuje. Odkazovaný objekt se nepoužívá přímo přes `weak_ptr`,
je nutné `weak_ptr` zkonvertovat na `shared_ptr` před přístupem na odkazovaný objekt.

```c++
std::weak_ptr<int> gw;

void observe() {
  std::cout << "use_count == " << gw.use_count() << ": ";
  if (auto spt = gw.lock()) { // Has to be copied into a shared_ptr before usage
    std::cout << *spt << "\n";
  } else {
    std::cout << "gw is expired\n";
  }
}

int main() {
  {
    auto sp = std::make_shared<int>(42);
    gw = sp;
    observe();
  }
  observe();
}
```

#### Konverze mezi smart pointers
Když vznikne potřeba sdíleně vlastnit něco dosud vlastněné unikátně, `unique_ptr` lze zkonvertovat
na `shared_ptr` (`template< class Y, class Deleter > shared_ptr( std::unique_ptr<Y,Deleter>&& r);`).

Pro přístup ke spravovanému objektu `weak_ptr` lze převést na `shared_ptr` (`std::shared_ptr<T>
weak_ptr::lock() const noexcept`, nebo `template< class Y > explicit shared_ptr( const
std::weak_ptr<Y>& r )`).

Nevím o bezpečném způsobu, jak převést `shared_ptr` na `unique_ptr`. Ale taková konverze logicky
stejně nedává smysl.

#### Smart pointers & thread safety
Je bezpečné dereferencovat, přiřazovat a rušit z více vláken různé instance `shared_ptr` a `weak_ptr` ukazující na stejný
objekt. Smart pointery neřeší thread safety práce se samotným odkazovaným objektem.

### Komunikace úmyslu s pomocí ukazatelů, referencí a nepovinných hodnot
Typový systém v moderním C++ nám umožňuje jednoznačně komunikovat vlastnosti parametrů nebo členských proměnných.
Ve starém kódu často vidíme předávání odkazů na objekty s pomocí ukazatelů;
ukazatele však neříkají nic o:
- přítomnosti objektu (ukazatel může být `nullptr`)
- vlastnictví (Je příjemce argumentu zodpovědný za uvolnění paměti? Může stejný objekt používat i
  někdo jiný?)
- počtu objektů (v některých případech se za ukazatelem skrývá celé pole)

S pomocí správného odkazování se na objekty můžeme snadno komunikovat všechny tyto koncepty:
- `T&`: Pokud očekáváme platný odkaz na objekt, který nevlastníme, použijeme (konstantní) referenci.
Pokud bez platného odkazu nedokážeme pracovat, měli bychom jej požadovat.
- `T`, `unique_ptr<T>`, `shared_ptr<T>`: Pokud získáváme vlastnictví objektu, použijeme předání hodnotou, případně smart pointery.
- `T*`: Pokud je možné, že předaný objekt odkazem není, použijeme holý ukazatel.
Holý ukazatel je nevlastnící a nepovinný odkaz.
(Tento úmysl by dobře komunikovalo `optional<T&>`, nicméně toto přetížení je ve standardní knihovně zakázané).
- `optional<T>`: Pro předávání nepovinnou hodnotou použijeme knihovní typ `optional`. Jednoznačně komunikujeme nepovinnost, navíc nepotřebujeme žádnou alokaci paměti navíc.
- Pole s pomocí `array<T, N>`, `vector<T>` v kombinaci s předešlými koncepty.

Uložení referencí v kontejnerech má problém: reference nelze převázat, a tudíž objekty, které je obsahují nelze kopírovat.
Tento problém lze obejít použitím `T*`, ale toto nás nutí "ručně" zaručit to, že ukazatel vždy ukazuje na nějaký platný objekt.
Pro tento účel obsahuje standardní knihovna typ `reference_wrapper<T>`, který slouží jako reference, kterou lze převázat na jiný objekt.

## const correctness
Správným používáním `const` lze předejít nechtěným změnám proměnných. Je vhodné dávat `const` všude,
kde nepotřebujeme možnost nějaká data modifikovat.

To platí pro:

-   proměnné - lepší, než makra, nepřicházíme o typovou kontrolu

```c++
const std::string name {"foo"};
name = "bar"s;                  // chyba zachycená překladačem
```

-   metody tříd - např. `bool std::vector::empty() const;` slibuje, že dotaz na "prázdnost" nezmění
  samotný objekt

-   parametry funkcí - například při předávání referencí často nechceme, aby volaná funkce měnila obsah

```c++
struct X {
    int a;
    int b;

    int operator<(const X& rhs) const {
        return std::tuple(a, b) < std::tuple(rhs.a, rhs.b);
    }
};
```


*const correctness* má vlastní [FAQ](https://isocpp.org/wiki/faq/const-correctness)

### mutable
Umožňuje změnit hodnotu nějaké členské proměnné i na konstantním objektu/v konstantní metodě.
Není určený k obcházení *constness*, označuje členské proměnné, jejichž změna neovlivní stav objekty viditelný zvenčí.

```c++
struct A {
    int x {0};
    mutable int z {0};

    void f() const {
        //x = 1;                  // error: assignment of member ‘A::x’ in read-only object
        z = 2;
    }
    void g() {
        x = 1;
        z = 2;
    }
};

int main(int argc, char *argv[])
{
    const A a;
    //a.x = 1;                    // error: assignment of member ‘A::x’ in read-only object
    a.z = 1;
    a.f();
    //a.g();                      // error: passing ‘const A’ as ‘this’ argument discards qualifiers [-fpermissive]
}
```

## constexpr
[`constexpr`](https://en.cppreference.com/w/cpp/language/constexpr) říká, že daný kód je možné
vyhodnotit během kompilace.

```c++
constexpr int square(int num) {
    return num * num;
}

int foo(int x) {
    int a = square(10);
    int b = square(x);
}
```

Viz [Godbolt](https://godbolt.org/z/7LYMi6) pro assembler.

## Lambda
Lambda umožňuje vytvářet anonymní funktor. Hodí se kdekoliv, kde potřebujeme vytvořit volatelný
objekt, ale vytvářet funkci nebo funktor by bylo zbytečné, např. jednorázová lokální utilitka,
*callable* pro algoritmus.

Struktura definice lambdy je v současnosti `[ captures ] ( params ) -> return_type { body }`.
V případě prázdného seznamu parametrů lze vynechat `( params )` a `-> return_type` není vždy nutné specifikovat.

```c++
std::vector<int> v {1, 3, 5, 6, 9, 7, 8, 2 };
// vrátí iterátor na první prvek beze zbytku dělitelný `2'
auto it = std::find_if(v.begin(), v.end(), [](int val) { return val % 2 == 0; });
```

### Captures
"Zachytí" lokální proměnné a zpřístupní je v těle lambdy. Lze zachytit hodnotou nebo referencí.
*Capture* lze udělat hodnotou (`[=]`, výchozí způsob) nebo referencí (`[&]`).

Proměnné zachycené referencí lze v lambdě měnit a změna se projeví zvenčí. Proměnné zachycené
hodnotou jsou *const*, to lze změnit definicí lambdy jako `mutable`.

```c++
  int i {0};
  
  //auto l1 = [i]() { i = 1; };     // error: assignment of read-only variable ‘i’
  auto l2 = [i]() mutable { i = 2; };
  auto l3 = [&i]() { i = 3; };

  l2();                       // `i` předané hodnotou, venku zůstane 0
  l3();                       // `i` předané referencí, i venku nová hodnota 3
```

Lambda výrazy bez capture lze přiřadit do ukazatele na funkci:
```c++
int (*my_fn_ptr)() = [] { return 5; };
```

Pokud v těle lambdy zachytáváme proměnné nebo reference na ně, vytvoří se místo jednoduché funkce tzv. funkční objekt, který už nelze přiřadit do ukazatele na funkci:
```c++
int x = 5;
// chyba při překladu
//int (*my_fn_ptr)() = [=] { return x; };
// std::function přebírá libovolnou funkci nebo funkční objekt, za cenu vyšší ceny invokace
std::function<int()> my_fn = [=] { return x; };
```

Zachycení objektu v metodě lze provést referencí nebo kopií (C++17).
```c++
struct Foo {
    int bar() const { return 5; }
    void foo() {
        // zachycení objektu Foo referencí
        auto x = [this] { return bar(); };
        // zachycení objektu Foo kopií
        auto y = [*this] { return bar(); };
    }
};
```

U referencí pozor na *dangling reference*.
```c++
std::function<int()> foo() {
    int a = 5, b = 6;
    return [&]{ return a + b; };
}
int bar() {
    auto f = foo();
    // proměnné a, b už neexistují, nedefinovaný výsledek f()
    return f();
}
```

### Generické lambdy (C++14)
Lambda výrazy lze definovat genericky s pomocí `auto`: operátor volání funkce je poté šablonový.

```c++
auto my_sum = [](auto a, auto b, auto c) { return a + b + c; };
double x = my_sum(5, 5.0, 6.5f);
int y = my_sum(5, 6, 7);
```

Existence generických lamb nám umožní použít `auto` místo opravdových typů.

```c++
template <typename T>
void sort_reversed(T& container) {
    std::sort(container.begin(), container.end(), [](auto&& lhs, auto&& rhs) { return rhs < lhs; });
}
```

## for / for-each / algoritmy
C++11 zavádí zkrácený zápis pro iteraci přes celou kolekci:

```c++
const std::vector<int> input {1, 2, 3, 4, 6, 8, 10};
std::vector<int> output;
auto double_it = [](auto&& val) { return val * 2; };

std::cout << "Input: " << input << '\n';
// tradičně
for (auto iter = input.begin(); iter < input.end(); ++iter) {
    output.push_back(double_it(*iter));
}
```

```c++
// "for each" loop
for (auto& val : input) {
    output.push_back(double_it(val));
}
```

Místo holých cyklů je lepší používat algoritmy z [Algorithms library], protože:
- vhodně zvolený algoritmus dává najevo záměr

```c++
std::accumulate(begin(a), end(a), 0.0);
// vs
for (int i = 0; i < v.size(); ++i) sum = sum + v[i];
```

- umožňuje získat paralelní zpracování "zadarmo", viz [execution policy]
```c++
std::transform(parallel_unsequenced_policy,
    input.begin(), input.end(), std::back_inserter(output), double_it);
```

Pro přehled o algoritmech viz [Algorithms library], nebo třeba přednášku [105 STL Algorithms in Less Than an Hour].

## structured bindings vs in-out parametry (C++17)
Pokud funkce vrací více než jednu hodnotu, je vhodné, aby opravdu vracela více hodnot.
V minulosti se často další hodnoty vracely přes parametry; tento způsob vracení hodnot není na první pohled zřejmý a závisí na dobré dokumentaci.

Příklad (strtol):
```c++
// C
extern "C" long int strtol(const char *str, char **endptr, int base);

// vs. C++
std::pair<long int, const char*> strtol(const char*, int base = 10);
```

S pomocí tzv. *structured bindings* dokážeme "rozložit" struktury, n-tice, atd. vrácené z funkcí na jednotlivé proměnné:
```c++
std::map<std::string, int> myMap;
// rozložíme std::pair na jeho složky; typické použití při vložení do mapy
auto [iterator, inserted] = myMap.insert({"foo", 5});
```

Můžeme rozkládat:
- nestatické membery třídy/struktury
- obyčejné pole `T[n]`
- n-ticový typ (např. `std::tuple`)

```c++
struct Foo {
    int a;
    float b;
};

Foo foo();

auto&& [a, b] = foo();
int cd[2] = {0, 5};
const auto [c, d] = cd;
// pozor: kvalifikátory před '[' (const, &, &&) se týkají skryté proměnné, nikoliv jednotlivých proměnných
int ee = 9;
float ff = 9.6;
// skutečné typy: int& e, float& f, ukazují na ee a ff
auto [e, f] = std::tie(ee, ff);
```

Tato jazyková podpora více návratových hodnot nám umožní pohodlně vracet více hodnot:
```c++
switch(auto&& [sensitivity, source] = settings.value<float>("sensitivity"); source)
{
    case SOURCE::NETWORK:
        std::cerr << "sensitivity set from network: " << sensitivity << "\n";
        // ...
    case SOURCE::DEFAULT:
    // ...
}
```

## Typy hodnot (Value categories)
Od C++11 kvůli zavedení `move` existují nové typy hodnot
- základní - *lvalue*, *xvalue* (expiring value) a *prvalue* (pure rvalue)

- smíšené (mixed) - *glvalue* (generalized lvalue) a *rvalue*

![Value categories](./value_categories.png)
zdroj: https://blog.knatten.org/2018/03/09/lvalues-rvalues-glvalues-prvalues-xvalues-help/


## Move

Použití `move` umožní lépe optimalizovat, když víme, že už nebudeme zdroj `move` potřebovat. Samotný
`move` nic nikam nepřesouvá, jen označí argument jak *rvalue reference*.
Aby třída podporovala *move semantics*, potřebujeme *move constructor* a *move copy assignment*, viz
dále.

V situacích, kdy zabere [copy elision], je lepší `move` nepoužívat. Typicky jde o vracení hodnot z
funkcí nebo dočasné kopie.

[Copy elision] může ovlivnit *observable behavior* tím, že vynechá vytváření a ničení dočasných
objektů, tzn. nelze spoléhat na zavolání *copy* a *move* konstruktorů a destruktoru.

## Forwarding reference

`auto&&`, `template <typename T> void foo(T&& t);`

Při použití `&&` u `auto`, případně u nějakého šablonového typu parametru `T`, nezískáme rvalue referenci, ale tzv. __forwarding referenci__.

```
auto&& vec = lvalue_nebo_rvalue_vyraz;
auto it = std::begin(vec);
++(*it);
```

Tato reference se naváže na r-hodnoty i l-hodnoty a `vec` bude mít odpovídající typ reference.
Pokud tuto referenci předáme dále a chceme využít jejího typu, můžeme použít `std::forward`:

```
auto&& foo = bar(); // vrací Foo, případně nějakou referenci na Foo
gaz(std::forward<Foo>(foo)); // je nutné specifikovat typ Foo, dedukce zde nefunguje)
```

Šablonové typy jsou forwarding reference pouze pokud jde o šablonový typ parametru;
reference na šablonové typy u struktur jsou klasické rvalue reference.

```
template <typename T>
struct X {
    void foo(X&& x); // x je rvalue reference
    template <typename Y>
    void bar(Y&& y); // y je forwarding reference
};
```

Příklad použití: "dekorátor" funkce `foo` s několika přetíženími:
```c++
int foo();
double foo(int);
bool foo(int, int, int);

template <typename... Args>
auto foo_decorated(Args&&... args) {
    std::cout << "Hello!\n";
    // perfektní forwarding každého parametru
    return foo(std::forward<Args>(args)...);
}

int main(int argc, char *argv[])
{
    std::cout << typeid(foo_decorated()).name() << std::endl;        // i
    std::cout << typeid(foo_decorated(1)).name() << std::endl;       // d
    std::cout << typeid(foo_decorated(1, 1, 2)).name() << std::endl; // b
}
```

## Enum class
```c++
enum class TrafficLight {
    GREEN,
    AMBER,
    RED,
};
enum class Apple {
    GREEN,
    RED,
    BLACK,
};

auto light = TrafficLight::GREEN;
auto apple = Apple::GREEN;
```

## [Static assert](https://en.cppreference.com/w/cpp/language/static_assert)
Compile-time assert. Užitečné pro kontrolu kdečeho, co je známé už při překladu, např. vlastnosti
typů (std::is_unsigned, std::is_function a asi std::is_*), maximální velikost pole, jakýkoliv
*constexpr*.

Syntaxe je

```c++
static_assert ( bool_constexpr , message )		// od C++11
static_assert ( bool_constexpr )		        // od C++17
```

## Konstrukce, inicializace a destrukce objektů

### =default, =delete
Od C++11 je možnost přinutit překladač, aby:

-   vygeneroval metody, které by jinak vynechal

    ```c++
    struct C {
        C() = default;
        C(int i);
    };
    
    C c;                        // bez `C() = default` by byl "error: no matching function for call to ‘C::C()’"
    ```

-   negeneroval automaticky generované metody
    ```c++
    class base {
    public:
        void f() {}
    };
    
    class child : public base {
    public:
        child() = delete;
        explicit child(int i) : i(i) {}
        void f() = delete;
    private:
        int i;
    };
    
    int main(int argc, char *argv[]) {
        base a;
        a.f();
        //child b;                    // error: use of deleted function ‘child::child()’
        child b(1);
        //b.f();                      // error: use of deleted function ‘void B::f()’
    }
    ```

  Mazání (`=delete`) se neomezuje jen na konstruktory / destruktory / přiřazovací operátory.

  Na viditelnosti *deleted* metod už nezáleží. Se staršími překladači (ještě GCC7) je lepší je dělat
  *public*. U *private* byly zavádějící chybové zprávy (`error: 'child::child()' is private within
  this context` vs. `error: use of deleted function 'child::child()'`), viz třeba
  [https://stackoverflow.com/a/57252425/3198741]().

### Konstruktory (a inicializace objektů obecně)
Vytvořený objekt má být validní (musí splňovat [class invariants]). Pokud konstruktor nemůže
vytvořit validní objekt, měl by vyhodit výjimku.

Podle signatury rozlišujeme druhy konstruktorů:
-   default - bez parametrů (`X::X()`)
-   copy (`X::X(const X&)`)
-   move (`X::X(X&&)`)
-   conversion - libovolný konstruktor s parametry, který není označený `explicit`

Do objektu lze taky přiřadit jiný objekt
-   copy assignment
-   move assignment

-   při inicializaci členských proměnných konstantní hodnotou je lepší používat *in-class
    initializers*, než je nastavovat v konstruktoru.

    ```c++
    struct X {
        std::string s1;             // default constructor => s1=""
        std::string s2 {"text"};
        int i1;                     // neinicializované, náhodná hodnota
        int i2 {};                  // default inicializace => i2=0
        int i3 {1};
    };
    ```

-   při inicializaci hodnotami předanými konstruktoru je lepší používat *braced init list*.

    ```c++
    struct X {
        std::string s;
    
        //X(std::string s_) { s = s_;} // takhle ne, zbytečná default konstrukce `s` následovaná copy assignment
        X(std::string s_) : s{s_} {}
    };
    ```

-   delegating constructor - umožňuje zavolat jiný konstruktor, není nutné duplikovat kód společný
    pro víc konstruktorů nebo volat z konstruktorů nějaké interní inicializační metody

    ```c++
    struct X {
        X(char a, int b) {/*...*/}
        X(int b) : X('a', b) {} // delegating constructor
    };
    ```

-   move constructor - vykrade druhý objekt. Původní objekt by měl zůstat v "nespecifikovaném, ale
    validním" stavu. Tzn. minimálně pro destrukci, přiřazení, ještě lépe by měly pořád platit [class
    invariants]. Příklad z [cppreference](https://en.cppreference.com/w/cpp/language/move_constructor).

    ```c++
    struct A
    {
        std::string s;
        int k;

        A(A&& o) noexcept :
               s(std::move(o.s)),       // explicit move of a member of class type
               k(std::exchange(o.k, 0)) // explicit move of a member of non-class type
        {}
    };
    ```
    
-   pozor na volání virtuálních metod v konstruktorech, *virtual dispatch* funguje jen pro to, co
    již bylo zkonstruováno, t.j. ne virtuální metody z potomků. Viz třeba [Stroustrup] 22.4. nebo
    [CPPcoreguidelines-C50].
    
-   single argument konstruktory explicit - předchází nechtěným implicitním konverzím

### Destruktory
Destruktory u bázových tříd definovat *public virtual*, nebo *protected non-virtual*, pro logiku za
tímto doporučením viz třeba [GOTW-Virtuality].

### rule of 3/5/0
Jde o sadu doporučení předcházejících běžným chybám ve správě zdrojů. Tato doporučení se netýkají
*default constructoru*.

#### Rule of three
Pokud má třída uživatelsky definovaný destructor, copy constructor nebo copy assignment operátor,
pravděpodobně potřebuje všechny tři.

#### Rule of five
Pokud má třída podporovat move semantics a má uživatelsky definovaný destructor, copy constructor
nebo copy assignment operátor, musí definovat i move constructor a move assignment operátor.

#### Rule of zero
Pokud třída potřebuje definovat kteroukoliv z metod z *rule of five*, měla by se zabývat jen správou
zdrojů. Pokud má logiku, neměla by explicitně řešit správu zdrojů. Viz [archivní
blogpost](https://web.archive.org/web/20121127171954/http://rmartinho.github.com/cxx11/2012/08/15/rule-of-zero.html)
pro detaily a zdůvodnění.

## override
Označí metodu objektu, že je děděná. Překladač zkontorluje, že v rodičovské třídě existuje virtuální
metoda se stejnou signaturou.

## std::variant
Jako typově bezpečnou náhrada `union` lze použít `std::variant`, při pokusu o čtení typu, který ve
`std::variant` není zrovna uložený, dojde k výjimce, zatímco u `union` to skrytě selže.

## Nástroje/hračky
### Online překladače
-   https://godbolt.org/
-   https://coliru.stacked-crooked.com/
-   https://www.onlinegdb.com/online_c++_compiler
-   a mnoho dalších, viz https://arnemertz.github.io/online-compilers/

## Další čtení
-   [isocpp.org](https://isocpp.org/) - rss, ne vše je nutné číst, ale dobrý rozcestník
-   stack exchange [newsletters](https://stackexchange.com/newsletters)
-   ithare - beta verze [knihy o vývoji her](http://ithare.com/contents-of-development-and-deployment-of-massively-multiplayer-games-from-social-games-to-mmofps-with-stock-exchanges-in-between/)
-   a spousta dalších různých blogů, třeba <https://www.bfilipek.com>, <https://www.fluentcpp.com/>, <https://herbsutter.com/category/c/gotw/>
-   literatura - viz třeba [The Definitive C++ Book Guide and List](https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list)
-   [CPP Core guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)
-   <https://github.com/AnthonyCalandra/modern-cpp-features>

# Možná příště
## Concurrency. parallelism
## Dedukce šablonových parametrů

## Reference
[Stroustrup] - Bjarne Stroustrup: The C++ Programming Language, 4th edition

[105 STL Algorithms in Less Than an Hour]: https://www.youtube.com/watch?v=2olsGf6JIkU
[Algorithms library]: https://en.cppreference.com/w/cpp/algorithm
[CPPcoreguidelines-C50]: http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c50-use-a-factory-function-if-you-need-virtual-behavior-during-initialization
[Class invariants]: https://en.wikipedia.org/wiki/Class_invariant
[Copy elision]: https://en.cppreference.com/w/cpp/language/copy_elision
[Execution policy]: https://en.cppreference.com/w/cpp/algorithm/execution_policy_tag_t
[GOTW-Virtuality]: http://www.gotw.ca/publications/mill18.htm
[Konstruktory]: #konstruktory-a-inicializace-objekt%C5%AF-obecn%C4%9B
[Stroustrup]: http://www.stroustrup.com/4th.html
[UB]: https://en.wikipedia.org/wiki/Undefined_behavior
[Under construction]: ./under_construction.jpg

## Budoucí standardy
### std::expected
### Consistent comparison (operator<=>)
