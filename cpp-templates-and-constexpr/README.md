# **C++ šablony a constexpr**

# Constexpr a metaprogramování

## Constexpr (*C++11*)
Specifikátor `constexpr` říká překladači, že definovaná funkce, konstruktor nebo proměnná je vyhodnotitelná už v době překladu (implikuje `inline`).
Tyto funkce či proměnné lze použít na místech, kde je nutná konstanta známá už během překladu, například šablonové parametry nebo jako velikost pole.
`constexpr` lze také použít při deklaraci proměnné pro označení známosti její hodnoty během překladu (implikuje `const`).

U šablon musí existovat alespoň jedna specializace, která splňuje podmínky pro `constexpr`, specializace které toto nesplňují nelze vyhodnotit v konstantním výrazu.

### constexpr proměnné
- musí mít typ z kategorie `LiteralType`: void, skalární typy, reference, ukazatele, pole LiteralType, třída s `constexpr` konstruktorem nebo agregát
- musí být okamžitě inicializovaná
- výraz, kterým ji inicializujeme, musí být konstantní výraz

### constexpr funkce
- vrací `LiteralType`
- všechny parametry jsou `LiteralType`
- **C++11**: tělo funkce se zkládá z:
  - prázdných statementů (samotný `;`)
  - `static_assert`
  - `typedef`, `using` deklarace, direktivy (`using namespace std;`, `using MyInt = int;`)
  - jeden `return` statement
- **C++14**: tělo funkce neobsahuje:
  - `goto`
  - labely (kromě `case` a `default`)
  - `try` blok (do C++20)
  - `asm` (ručně doplněný asembler)
  - definici proměnné, která není typu `LiteralType` nebo není inicializovaná
  - definici statické proměnné, modifikaci globální proměnné
- rekurzivně volané funkce musí být také `constexpr`

### consexpr konstruktor
- všechny parametry jsou `LiteralType`
- třída nedědí virtuálně
- splňuje podmínky pro constexpr funkci
- volané konstruktory nadtříd musí být `constexpr`

---

## **Příklad**
Implementujte constexpr funkce `constexpr unsigned range_sum(unsigned i, unsigned j);` a `constexpr unsigned cpp11_range_sum(unsigned i, unsigned j);` (stejná funkce, jen přeložitelná a spustitelná v `C++11`).
- Funkce spočítá součet všech čísel `i + (i+1) + ... + j`.
- Pokud je `i` větší než `j`, vyvoláme výjimku `std::invalid_argument`.

Test:
```c++
static_assert(cpp11_range_sum(1, 9) == 45, "wrong range_sum result");
// C++14 and up
static_assert(range_sum(1, 9) == 45);
```

---

## Constexpr vs šablonové metaprogramování
Constexpr nám umožňuje implementovat čitelně a udržovatelně funkce, které bylo dříve nutné implementovat šablonovým metaprogramováním.

Příklad: faktoriál vyhodnotitelný během překladu
```c++
// template metaprogramming (C++98)
template <unsigned i>
struct cpp98_factorial {
    enum {
        value = i * cpp98_factorial<i-1>::value
    };
};
template <>
struct cpp98_factorial<0> {
    enum {
        value = 1
    };
};
// constexpr
constexpr unsigned factorial(unsigned i)
{
    if (i <= 1)
        return 1;
    return i * factorial(i - 1);
}

static_assert(factorial(4) == cpp98_factorial<4>::value);

```

# Šablony
- https://en.cppreference.com/w/cpp/language/templates
- https://en.cppreference.com/w/cpp/language/class_template
- https://en.cppreference.com/w/cpp/language/function_template
- https://en.cppreference.com/w/cpp/language/type_alias
- https://en.cppreference.com/w/cpp/language/variable_template

Šablony popisují rodiny tříd, funkcí, typových aliasů (c++11) a proměnných (c++14).

## Šablonové parametry
Šablony jsou parametrizované:
- typovými šablonovými parametry (`typename T`, `class T`)
- netypovými šablonovými parametry (`int n`, `bool b`)
- šablonovými šablonovými parametry (`template <typename> typename Container`)

Pro vytvoření instance šablony (specializace) je nutné, aby byly všechny parametry uvedené, dedukované, nebo měly implicitní hodnotu (`template <typename T = int>`).

## `typename`, `template`
Kompilátor nedokáže z typových parametrů odvodit, jestli prvky obsazené v jejich jmenném prostoru jsou hodnoty, typy nebo šablony.
Implicitně proto předpokládá hodnoty a v případě, že to tak není, je v místě použití nutné specifikovat, o kterou z těchto věcí jde.

```c++
template <class T>
struct Foo {
    // T::value is a static value
    int i = T::value;
    // T::value_type is a type
    using value_type = typename T::value_type;
    // T::template_type is a templated type or type alias
    template <typename U>
    using template_type = typename T::template template_type<U>;
};
```

## Dedukce šablonových parametrů

### Deduction guides
- https://en.cppreference.com/w/cpp/language/class_template_argument_deduction

Od C++14 funguje dedukce šablonových argumentů i pro volání konstruktorů.
Pokud jsou šablonové argumenty odvoditelné z konstruktoru, překladač to udělá. Například pokud konstruktor dostane jeden parametr `const T&`, dokáže snadno automaticky odvodit `T`.

U některých konstruktorů dokážeme snadno dedukovat výsledný typ, ovšem netriviálně, případně jinak než automaticky.
Takovouto dedukci dokážeme specifikovat s pomocí tzv. **deduction guides**.

```c++
// volitelně i explicit
název ( parametry ) -> název<šablonové-parametry> ;		
```

Tímto konstruktem dokážeme specifikovat, které šablonové parametry se automaticky vydedukují při použití tohoto konstruktoru. Pozor, při explicitním uvedení šablonových parametrů k žádné dedukci nedochází.

---
### **Příklad**
napište třídu `Formatter<T>`, která vytvoří stringovou reprezentaci prvku nebo několika prvků T.
V konstruktoru dostane buď jeden prvek, nebo několik prvků předaných párem iterátorů. Přístupný je poté `const std::string& Formatter::str() const noexcept;` a kopie předaných prvků v konstruktoru jako vektor `const std::vector<T>& Formatter::elements() const noexcept`

(Implementaci metod ukládání do řetězce např. přes stringstream a kopírování do vektoru můžeme přeskočit.)

**Napište deduction guide tak, aby uživatel nemusel při použití iterátorového konstruktoru ručně specifikovat šablonový typ.**

*hint: `std::iterator_traits<Iter>::value_type` v `<iterator>`*

---


## Specializace


### Úplná specializace
- https://en.cppreference.com/w/cpp/language/template_specialization

Úplná specializace nám umožní explicitně definovat specializaci šablonové funkce, třídy nebo proměnné (+ další) pro konkrétní typ.

syntax:
```c++
template <typename T>
constexpr bool is_int = false;

template <>
constexpr bool is_int<int> = true;
```

### Částečná specializace
- https://en.cppreference.com/w/cpp/language/partial_specialization

Částečnou specializaci můžeme provádět pro šablonové třídy a proměnné.
Pokud známe nějaký vzor argumentů, pro který dokážeme nebo chceme třídu nebo proměnnou specializovat, provedeme částečnou specializací.

syntax:
```c++
template <typename T, typename U>
constexpr bool is_same = false;

template <typename T>
constexpr bool is_same<T, T> = true;
```

### Částečné uspořádání nad specializacemi
Z množiny specializací vybere kompilátor vždy tu nejvíce specializovanou. Aplikovatelná úplná specializace se vybere před částečnou specializací, a méně obecná částečná specializace se vybere před více obecnou.

```c++
template <typename T1, typename T2>
constexpr int x = 0;

template <>
constexpr unsigned x<int, int> = 3;

template <typename T>
constexpr bool x<T, T> = true;

template <typename T>
constexpr double x<int, T> = 2.5;
// ---
static_assert(x<bool, int> == 0);
static_assert(x<bool, bool>);
static_assert(x<int, bool> == 2.5);
static_assert(x<int, int> == 3);
```

---
## **Příklad**
Vytvořte šablonovou proměnnou `constexpr bool is_subclass<Child, Parent>`. Proměnná má hodnotu `true` právě tehdy, když je třída Child podtřídou Parent nebo jsou obě třídy stejné.

Pomocné struktury a/nebo funkce můžete umístit do `namespace detail`.

*hint: implicitní konverze ukazatelů vs parametr `...`, je nutné umístit obě varianty do struktury*

Vytvořte šablonovou proměnnou `is_same<T1, T2>`, která má hodnotu true právě když jsou oba šablonové parametry stejný typ.

---

### `decltype(x)`
Klíčové slovo `decltype` nám umožní získat typ výrazu.

```c++
// proměnná typu shodného s návratovou hodnotou foo
decltype(foo()) x = foo();
```

## dedukovaný návratový typ s pomocí `auto` a `decltype(auto)`
Pokud předem neznáme návratový typ funkce, můžeme jej nechat překladač dedukovat automaticky.

```c++
int x = 42;

auto foo() { return x; } // return type is int
const auto& bar() { return x; } // return type is const int&
// return type is the same as decltype(return_expression)
decltype(auto) baz() { return x; } // return type is int
decltype(auto) qux() { return (x); } // return type is int&
// we can use explicit trailing type annotation
auto quux() -> const int& { return (x); } // return type is const int&

```

# Variadické šablony (C++11)
- https://en.cppreference.com/w/cpp/language/parameter_pack

V některých případech nedokážeme předem určit, kolik šablonových parametrů potřebujeme přijímat.
Ať už implementujeme n-tice, nebo chceme posílat libovolný počet hodnot nějakému příjemci.

Šablona, která přijímá jeden nebo více balíčků parametrů, se nazývá **variadická šablona**.

Ukázka:
```c++
template <typename... Types>
struct Tuple {};

template <int... vals>
struct Array {
    int values[sizeof...(values)] = {vals...};
};
```

## Parameter pack
**Parameter pack** je šablonový parametr který přijímá nula nebo více šablonových parametrů. Může jít o typy, hodnoty nebo šablony.
Parameter pack se musí nacházet na konci sekce šablonových parametrů:
```c++
// template <typename... Ts, typename T> // error
template <typename T, typename... Ts> // OK
struct Foo {};

template <bool... vals, typename... Ts> // OK
struct Bar {};
```

Syntax:
```c++
template <typename... Ts>
```
Parameter pack přijímající 0..n typů.
```c++
template <int... vals>
```
Parameter pack přijímající 0..n integerů.
```c++
template <typename... Ts>
void foo(Ts... ts);
```
Funkce foo přijímá 0..n parametrů s typy odpovídajícím šablonovým typům v `Ts`.
```c++
template <typename... Ts>
void foo(Ts&&... ts);
```
Funkce foo přijímá 0..n parametrů s typy odpovídajícím forwarding referencím na šablonové typy v `Ts`.
```c++
template <typename... Ts>
void foo(Ts&&... ts) {
    bar(std::forward<Ts>(ts)...);
}
```
`vzor ...` se rozloží na čárkou oddělený seznam nula nebo více vzorů. Tento vzor musí obsahovat alespoň jeden parameter pack.

## `sizeof...(T)`
Operátor `sizeof...` nám umožní získat počet pvků v parameter packu.

---
## **Příklad**
Napište funkci `announced_vector<T, Ts...>`.
Funkce na standardní výstup oznámí vytvoření vektoru a počet parametrů a vrátí `std::vector<T>` s konstruktorem, který odpovídá parametrům poslaným do `announced_vector`.

---


## Fold expressions (C++17)
- https://en.cppreference.com/w/cpp/language/fold

Fold expressions nám umožňují redukovat parameter pack přes binární operátor.
Můžeme definovat fold expression s nulovou hodnotou, nebo bez ní.

Syntax:
```c++
template <typename... Ts>
auto foo(Ts&&... args) {
    return
        // unary left fold
        (... * args) /
        // unary right fold
        (args + ...) +
        // binary left fold
        (10'000 | ... | args) -
        // binary right fold
        (args && ... && 42);
}

template<typename ...Args>
void printer(Args&&... args) {
    (std::cout << ... << args) << '\n';
}
```

---
## **Příklad**
Napište `constexpr` variadickou šablonovou funkci `average`.
Přijme libovolné množství argumentů a vypočítá aritmetický průměr.

Napište `constexpr` variadickou šablonovou funkci `none_of`.
Přijme libovolné množství argumentů konvertovatelných na bool a provede logické `&&` nad negacemi výrazů konvertovaných na bool.
V případě prázdného seznamu argumentů má funkce hodnotu `true`.

Napište `constexpr` variadickou šablonovou proměnnou `average_sizeof`, která má hodnotu průměrného `sizeof` svých šablonových typů.

---

# SFINAE
- https://en.cppreference.com/w/cpp/language/sfinae

Substitution Failure Is Not An Error.

Pro výběr z nabídky přetížených šablonových funkcí není chyba dosazení typu chybou v překladu a pouze vyloučí danou specializaci.
Toto nám umožní podmíněně vybírat přetížené varianty různých funkcí.
Kompletní výčet SFINAE chyb viz. odkaz.

Příklad: voláme různé verze funkce pro celočíselné a floating point typy.
```c++
#include <type_traits>

template<typename T>
std::enable_if_t<std::is_integral_v<T>, T> f(T t){
    //integral version returning T
}
template<typename T>
std::enable_if_t<std::is_floating_point_v<T>, T> f(T t){
    //floating point version returning T
}
```

## `std::enable_if_t`
`std::enable_if_t` přijme dva šablonové argumenty: první je boolan hodnota a druhá je typ.
Pokud je hodnota `true`, dostaneme tento typ.
V opačném případě není typ definovaný.

Příklad výše.

## `std::void_t` (C++17)
`std::void_t` přijme libovolný počet šablonových typů a stane se z něj typ `void`.

Umožňuje nám snadno specializovat podle existence typů:
```c++
template <typename T, typename = void>
constexpr bool iterable = false;

template <typename T>
constexpr bool iterable<T, std::void_t<
        decltype(std::declval<T>().begin()),
        decltype(std::declval<T>().end())
    >> = true;
```

## `declval<T>()` a `auto -> decltype(expr, void())`
Pro kontrolu existence některých operací potřebujeme často získat hodnotu daného typu (v kontextu vyhodnocení typu). Nemůžeme se u žádného typu spolehnout na existenci konkrétního typu, ale můžeme získat imaginární referenci na hodnotu tohoto typu.

Knihovní funkce `declval<T>()` vrátí rvalue referenci na typ `T`. Nelze ji zavolat v opravdovém kódu, ale lze ji využít v konstantním kontextu.

Pro omezení šablonových funkcí lze využít decltype idiom.
Návratový typ šablonové funkce umístíme nakonec.
V první části decltype vynutíme existenci volání funkce, operátoru atd. a použijeme operátor čárka pro opravdový návratový typ.
Pro návratový typ uvnitř decltype můžeme použít právě `declval<T>`.

```c++
// volatelné jen pro typy, kde lze volat velikost
template <typename T>
auto foo(const T& t) -> decltype(t.size(), void()) {
    // ...
}

template <typename T>
auto bar(const T& t) -> decltype(t.empty(), t.begin()) {
    // ...
    return t.begin();
}
```

---
## **Příklad**
1) Napište implementaci `std::enable_if_t`.

2) Napište šablonovou proměnnou `constexpr bool convertible<T, Y>`, která má hodnotu `true`, pokud lze `T` konvertovat na `Y`.

Pomocné funkce, struktury nebo proměnné umístěte do `namespace detail`. Šablony v globálním prostoru by měla mít právě dva šablonové parametry.

*Hint pro 2: pomocná proměnná pro std::void_t specializaci, decltype a declval*

---

## Alternativy
SFINAE je pomalé, ošklivě a špatně čitelné.

Toto jsou některé alternativy, které nám někdy umožní se těmto věcem vyhnout.

### Tag Dispatch
```c++
#include <iostream>
#include <vector>
#include <list>
#include <iterator>
 
template <class BDIter>
void alg(BDIter, BDIter, std::bidirectional_iterator_tag)
{
    std::cout << "alg() called for bidirectional iterator\n";
}
 
template <class RAIter>
void alg(RAIter, RAIter, std::random_access_iterator_tag)
{
    std::cout << "alg() called for random-access iterator\n";
}
 
template <class Iter>
void alg(Iter first, Iter last)
{
    alg(first, last,
        typename std::iterator_traits<Iter>::iterator_category{});
}
 
int main()
{
    std::vector<int> v;
    alg(v.begin(), v.end());
 
    std::list<int> l;
    alg(l.begin(), l.end());
 
//    std::istreambuf_iterator<char> i1(std::cin), i2;
//    alg(i1, i2); // compile error: no matching function for call
}
```

### `static_assert`
Pokud chceme omezit některé vlastnosti šablonového typu a nevybíráme z více variant, často bude stačit `static_assert` místo `enable_if`.

### Koncepty (C++20)
- https://en.cppreference.com/w/cpp/language/constraints

Definujeme požadavky na šablonový typ, kompilátor je zkontroluje a případné chyby hlásí na méně než 50 řádků.

# Velký příklad:
Implemetujte kontejner `narray`, který reprezentuje n-rozměrné pole.
Kontejner má shodný interface se zanořenými std::array odpovídajících rozměrů. Prvky pole **nejsou** dynamicky alokované, nachází se v poli na stacku.

Pro implementaci můžete použít už existující prostředky ze standardní knihovny.

Řešení 1 pro inicializaci: implementujte konstruktor přijímající std::initializer_list<T> a zkontrolujte jeho správnou velikost.
Řešení 2 pro inicializaci: Inicializujeme čistou aggregate inicialization přesně jako `std::array`.

```c++
// použití
int main() {
    // typ, dimenze 1, dimenze 2, ...
    narray<int, 3, 3> arr = {0, 1, 2,
                             3, 4, 5,
                             6, 7, 8};

    arr[1][0] = 5;

    for (auto&& arrays: arr) {
        for (auto&& elem: arrays) {
            std::cout << elem << ", ";
        }
        std::cout << "\n";
        arrays.fill(0);
    }

    return arr[1][0];
}
```

# Velký dobrovolný příklad (spíše na doma):
Implementujte alespoň jednoduchý n-ticový `tuple` typ, inspirujte se `std::tuple`.

Návod:
- http://blogs.microsoft.co.il/sasha/2015/01/12/implementing-tuple-part-1/
- http://blogs.microsoft.co.il/sasha/2015/01/16/implementing-tuple-part-2/
- ... ostatní díly série
