#include <stdexcept>
#include <string>
#include <iterator>
#include <iostream>
#include <sstream>
#include <list>
#include <vector>

template <typename T>
class Formatter {
public:
    Formatter(const T& elem) {
        std::stringstream stream;
        m_elements.push_back(elem);
        stream << elem;
        m_str = stream.str();
    }

    template <typename Iter>
    Formatter(Iter begin, Iter end): m_elements(begin, end) {
        std::stringstream stream;
        for (auto&& e: m_elements) {
            stream << e << ", ";
        }
        m_str = stream.str();
        if (!m_elements.empty()) {
            m_str.pop_back();
            m_str.pop_back();
        }
    }

    const std::string& str() const noexcept {
        return m_str;
    }
    const std::vector<T>& elements() const noexcept {
        return m_elements;
    }

private:
    std::vector<T> m_elements;
    std::string m_str;
};

template <typename Iter>
Formatter(Iter, Iter) -> Formatter<typename std::iterator_traits<Iter>::value_type>;

int main() {
    std::list<int> list = {5, 10, 15};
    Formatter f(list.begin(), list.end());
    std::cout << f.str() << "\n";
    return 0;
}
