#include <iostream>
#include <type_traits>

namespace detail {
template <typename T>
struct is_subclass {
    static constexpr bool test(...) { return false; }
    static constexpr bool test(T*) { return true; }
};

}

template <typename Child, typename Parent>
constexpr bool is_subclass = detail::is_subclass<Parent>::test(static_cast<Child*>(nullptr));

struct A {};
struct B: A {};
struct C: B {};

struct Z: A {};

static_assert(is_subclass<C, B>);
static_assert(is_subclass<C, A>);
static_assert(!is_subclass<A, C>);
static_assert(is_subclass<Z, A>);
static_assert(!is_subclass<Z, B>);

int main() {
    return 0;
}
