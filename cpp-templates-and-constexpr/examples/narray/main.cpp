#include <array>
#include <iostream>
#include <stdexcept>

#if false
// initializer list
template <typename T, std::size_t n, std::size_t... ns>
class narray: public std::array<narray<T, ns...>, n> {
public:
    static_assert(n != 0);

    using std::array<narray<T, ns...>, n>::array;

    constexpr narray(std::initializer_list<T> il) {
        static_assert(sizeof(int[n]) == sizeof(std::array<int, n>));
        if (il.size() != (n * ... * ns)) {
            throw std::invalid_argument("Bad initializer size");
        }
        auto it = il.begin();
        T* arr = reinterpret_cast<T*>(this->data());
        for (std::size_t i = 0; i < (n * ... * ns); ++i, ++it) {
            arr[i] = *it;
        }
    }
};

template <typename T, std::size_t n>
class narray<T, n>: public std::array<T, n> {
public:
    using std::array<T, n>::array;

    constexpr narray(std::initializer_list<T> il) {
        static_assert(sizeof(int[n]) == sizeof(std::array<int, n>));
        if (il.size() != n) {
            throw std::invalid_argument("Bad initializer size");
        }
        T* arr = this->data();
        auto it = il.begin();
        for (std::size_t i = 0; i < n; ++i, ++it) {
            arr[i] = *it;
        }
    }
};
#else
// aggregate initialization
namespace detail {
    template <typename T, std::size_t n, std::size_t... ns>
    struct narray {
        using type = std::array<typename narray<T, ns...>::type, n>;
    };

    template <typename T, std::size_t n>
    struct narray<T, n> {
        using type = std::array<T, n>;
    };
}

template <typename T, std::size_t n, std::size_t... ns>
using narray = typename detail::narray<T, n, ns...>::type;
#endif


int main() {
    // typ, dimenze 1, dimenze 2, ...
    narray<int, 3, 3> arr = {0, 1, 2,
                             3, 4, 5,
                             6, 7, 8};

    arr[1][0] = 5;

    for (auto&& arrays: arr) {
        for (auto&& elem: arrays) {
            std::cout << elem << ", ";
        }
        std::cout << "\n";
    }

    return arr[1][0];
}
