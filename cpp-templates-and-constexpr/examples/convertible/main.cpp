#include <iostream>
#include <type_traits>

namespace detail {
template <bool b, typename T>
struct enable_if {};

template<typename T>
struct enable_if<true, T> {
    using type = T;
};

template <typename T, typename Y, class = void>
constexpr bool convertible = false;

template <typename T, typename Y>
constexpr bool convertible<T, Y, std::void_t<decltype(Y(std::declval<T>()))>> = true;
}

template <typename T, typename Y>
constexpr bool convertible = detail::convertible<T, Y>;

template <bool b, typename T>
using enable_if_t = typename detail::enable_if<b, T>::type;


// test
struct X{ X() = delete; operator bool(); };
struct Y{};

static_assert(convertible<int, bool>);
static_assert(convertible<bool, int>);
static_assert(convertible<X, bool>);
static_assert(!convertible<Y, bool>);
static_assert(!convertible<double, void*>);

template <typename T>
enable_if_t<std::is_integral_v<T>, T> foo(T t) { return t; }

int main() {
    std::cout << std::boolalpha
              << "convertible<int, bool> == " << convertible<int, bool>
              << "\nconvertible<X, bool> == " << convertible<X, bool>
              << "\nconvertible<Y, bool> == " << convertible<Y, bool>
              << "\n";
    foo(5);
    // error
    //foo(5.5);
    return 0;
}
