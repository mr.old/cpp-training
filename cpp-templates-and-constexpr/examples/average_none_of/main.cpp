#include <iostream>
#include <type_traits>

template <typename... Ts>
constexpr auto average(const Ts&... args) {
    return (args + ...) / sizeof...(Ts);
}

template <typename... Ts>
constexpr bool none_of(const Ts&... args) {
    return (!(static_cast<bool>(args)) && ... && true);
}

template <typename... Ts>
constexpr std::size_t average_sizeof = (sizeof(Ts) + ...) / sizeof...(Ts);

static_assert(average(5) == 5);
static_assert(average(1.0, 5, 1, 3) == 2.5);

static_assert(none_of(false));
static_assert(none_of(false, 0, 0.0));
static_assert(!none_of(true));
static_assert(!none_of(false, 42, 0.0));
static_assert(none_of());

struct X {};

static_assert(average_sizeof<char, unsigned char> == sizeof(char));
static_assert(average_sizeof<X[100], unsigned char> == 50);

int main() {
    return 0;
}
