#include <iostream>
#include <type_traits>
#include <vector>

template <typename T, typename... Ts>
std::vector<T> announced_vector(Ts&&... args) {
    std::cout << "Behold! We construct a vector with "
              << sizeof...(Ts)
              << " arguments in the constructor!\n";
    return std::vector<T>(std::forward<Ts>(args)...);
}

int main() {
    auto vec1 = announced_vector<double>(5, 6.0);
    auto vec2 = announced_vector<double>();
    return 0;
}
