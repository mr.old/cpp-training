#include <stdexcept>

constexpr unsigned cpp11_range_sum(unsigned i, unsigned j) {
    return i > j
        ? throw std::invalid_argument("I is larger than J.")
        : i == j
            ? i
            : i + cpp11_range_sum(i + 1, j);
}

#if __cplusplus >= 201402L
constexpr unsigned range_sum(unsigned i, unsigned j) {
    if (i > j) {
        throw std::invalid_argument("I is larger than J.");
    }
    unsigned result = 0;
    for (; i <= j; ++i) {
        result += i;
    }
    return result;
}
#else
#define range_sum cpp11_range_sum
#endif

int main() {
    static_assert(cpp11_range_sum(1, 9) == 45, "");
    static_assert(range_sum(1, 9) == 45, "");
    return 0;
}
